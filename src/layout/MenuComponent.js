/* eslint-disable react/prop-types */
/* eslint-disable no-underscore-dangle */
import React, { useContext } from "react";
import { v4 as uuidv4 } from "uuid";
import { findWrapping } from "prosemirror-transform";
import { WaxContext } from "wax-prosemirror-core";
import styled from "styled-components";

const Menu = styled.div`
  background: #fff;
  border-bottom: 1px solid #ebebf0;
  border-top: 1px solid #ebebf0;
  display: flex;
  min-height: 40px;
  user-select: none;
  width: 100%;
  z-index: 999;

  > div:not(:last-child) {
    border-right: 1px solid #ebebf0;
  }

  > div:last-child {
    border-left: 1px solid #ebebf0;
    margin-left: auto;
  }
  svg {
    fill: black;
  }
`;

const MenuToolGroup = styled.div`
  border-right: 1px solid;
  align-items: center;
  display: flex;
  padding: 0 4px;
  button {
    margin: 0 5px;
  }
`;

const ResponseTypeButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
  }
`;

const MenuComponent = () => {
  const {
    activeView,
    pmViews: { main },
    app,
  } = useContext(WaxContext);

  const Base = app.container.get("Base");
  const Annotations = app.container.get("Annotations");
  const Lists = app.container.get("Lists");
  const Images = app.container.get("Images");
  const Tables = app.container.get("Tables");
  const FullScreen = app.container.get("FullScreen");

  const isDisabledResponseType = () => {
    const { disallowedTools } = activeView.props;
    const { from } = activeView.state.selection;
    if (from === null || disallowedTools.includes("ResponseType")) return true;
    return false;
  };

  const handleMouseDown = () => {
    const { dispatch, state } = main;
    const { tr } = state;
    const { $from, $to } = state.selection;
    const range = $from.blockRange($to);

    const wrapping =
      range &&
      findWrapping(range, state.config.schema.nodes.fill_the_gap_container, {
        id: uuidv4(),
      });
    if (!wrapping) return false;
    tr.wrap(range, wrapping);
    dispatch(tr);
  };

  return (
    <Menu>
      <MenuToolGroup>
        {Base._tools
          .filter((tool) => tool.name !== "Save")
          .map((tool) => tool.renderTool(activeView))}
      </MenuToolGroup>
      <MenuToolGroup>
        {Annotations._tools
          .filter((tool) => tool.name !== "Code")
          .map((tool) => tool.renderTool(activeView))}
      </MenuToolGroup>
      <MenuToolGroup>
        {Lists._tools.map((tool) => tool.renderTool(activeView))}
      </MenuToolGroup>
      <MenuToolGroup>
        {Images._tools.map((tool) => tool.renderTool(activeView))}
      </MenuToolGroup>

      <MenuToolGroup>
        {Tables._tools.map((tool) => tool.renderTool(activeView))}
      </MenuToolGroup>
      <MenuToolGroup>
        <ResponseTypeButton
          disabled={isDisabledResponseType()}
          onMouseDown={handleMouseDown}
        >
          Response Type
        </ResponseTypeButton>
      </MenuToolGroup>
      <MenuToolGroup>
        {FullScreen._tools.map((tool) => tool.renderTool(activeView))}
      </MenuToolGroup>
    </Menu>
  );
};

export default MenuComponent;

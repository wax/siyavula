import React, { useContext } from "react";
import styled, { ThemeProvider } from "styled-components";
import { WaxContext } from "wax-prosemirror-core";
import EditorElements from "./EditorElements";
import MenuComponent from "./MenuComponent";
import cokoTheme from "./cokoTheme";
import "wax-prosemirror-core/dist/index.css";
import "wax-prosemirror-services/dist/index.css";

const Wrapper = styled.div`
  background: white;
  display: flex;
  flex-direction: column;
  font-family: "Arial";
  font-size: 14px;
  height: 100%;
  position: absolute;
  overflow: hidden;
  width: 100%;

  * {
    box-sizing: border-box;
  }
`;

const Main = styled.div`
  display: flex;
  flex-grow: 1;
  height: calc(100% - 40px);
`;

const EditorArea = styled.div`
  background: #f4f4f7;
  flex-grow: 1;
`;

const WaxSurfaceScroll = styled.div`
  box-sizing: border-box;
  display: flex;
  height: 100%;
  overflow-y: auto;
  padding: 25px 25% 0 25%;
  position: relative;
  width: 100%;

  /* PM styles  for main content*/
  ${EditorElements};
`;

const EditorContainer = styled.div`
  height: 100%;
  position: relative;
  width: 100%;

  .ProseMirror {
    box-shadow: 0 0 8px #ecedf1;
    min-height: 100%;
    padding: 20px;
  }
`;

const Layout = ({ editor }) => {
  const {
    pmViews: { main },
    options: { fullScreen },
  } = useContext(WaxContext);

  let fullScreenStyles = {};

  if (fullScreen) {
    fullScreenStyles = {
      backgroundColor: "#fff",
      height: "100%",
      left: "0",
      margin: "0",
      padding: "0",
      position: "fixed",
      top: "0",
      width: "100%",
      zIndex: "999",
    };
  }

  return (
    <ThemeProvider theme={cokoTheme}>
      <Wrapper style={fullScreenStyles} id="wax-container">
        {main && <MenuComponent />}

        <Main>
          <EditorArea>
            <WaxSurfaceScroll>
              <EditorContainer>{editor}</EditorContainer>
            </WaxSurfaceScroll>
          </EditorArea>
        </Main>
      </Wrapper>
    </ThemeProvider>
  );
};

export default Layout;

import { Service } from "wax-prosemirror-core";
import fillTheGapContainerNode from "./schema/fillTheGapContainerNode";
import fillTheGapNode from "./schema/fillTheGapNode";
import ResponseTypeContainerNodeView from "./ResponseTypeContainerNodeView";
import FillTheGapNodeView from "./FillTheGapNodeView";
import ResponseTypeContainerComponent from "./components/ResponseTypeContainerComponent";
import GapComponent from "./components/GapComponent";

class ResponseTypeService extends Service {
  name = "ResponseTypeService";

  boot() {
    console.log("in boot");
  }

  register() {
    const createNode = this.container.get("CreateNode");
    const addPortal = this.container.get("AddPortal");

    createNode({
      fill_the_gap_container: fillTheGapContainerNode,
    });

    createNode({
      fill_the_gap: fillTheGapNode,
    });

    addPortal({
      nodeView: ResponseTypeContainerNodeView,
      component: ResponseTypeContainerComponent,
      context: this.app,
    });

    addPortal({
      nodeView: FillTheGapNodeView,
      component: GapComponent,
      context: this.app,
    });
  }
}

export default ResponseTypeService;

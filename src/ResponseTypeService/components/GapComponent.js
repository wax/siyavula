import React, { useContext, useRef, useState } from "react";
import styled from "styled-components";
import { WaxContext, DocumentHelpers } from "wax-prosemirror-core";

const GapComponent = ({ node, view, getPos }) => {
  const {
    pmViews: { main },
  } = useContext(WaxContext);

  const inputRef = useRef(null);
  const [inputValue, setInputValue] = useState(node.attrs.value);

  const onChange = () => {
    setInputValue(inputRef.current.value);
    const allNodes = getNodes(main);
    allNodes.forEach((singleNode) => {
      if (singleNode.node.attrs.id === node.attrs.id) {
        main.dispatch(
          main.state.tr.setNodeMarkup(singleNode.pos, undefined, {
            ...singleNode.node.attrs,
            value: inputRef.current.value,
          })
        );
      }
    });
  };

  return (
    <input
      data-type={node.attrs.type}
      onChange={onChange}
      ref={inputRef}
      type="text"
      value={inputValue}
    ></input>
  );
};

export default GapComponent;

const getNodes = (main) => {
  const allNodes = DocumentHelpers.findInlineNodes(main.state.doc);
  const fillTheGapNodes = [];
  allNodes.forEach((node) => {
    if (node.node.type.name === "fill_the_gap") {
      fillTheGapNodes.push(node);
    }
  });
  return fillTheGapNodes;
};

import React, { useContext } from "react";
import { v4 as uuidv4 } from "uuid";
import styled from "styled-components";
import { WaxContext } from "wax-prosemirror-core";
import ContainerEditor from "./ContainerEditor";

const ResponseTypeContainer = styled.div`
  border: 3px solid #f5f5f7;
  margin-bottom: 30px;
`;

const ResponseTypeContainerTool = styled.div`
  border: 3px solid #f5f5f7;
  border-bottom: none;

  span {
    position: relative;
    top: 3px;
  }
`;

const ResponseTypeWrapper = styled.div`
  margin: 0px 38px 15px 38px;
  margin-top: 10px;
`;

const ResponseTypeContainerComponent = ({ node, view, getPos }) => {
  const { activeView } = useContext(WaxContext);

  const addResponseType = (e, type) => {
    e.preventDefault();
    const { state, dispatch } = activeView;
    const createGap = state.config.schema.nodes.fill_the_gap.create({
      id: uuidv4(),
      type: type,
    });

    dispatch(state.tr.replaceSelectionWith(createGap));
  };

  return (
    <ResponseTypeWrapper>
      <ResponseTypeContainerTool>
        <div>
          <button onMouseDown={(e) => addResponseType(e, "type1")}>
            Response Type 1
          </button>
          <button onMouseDown={(e) => addResponseType(e, "type2")}>
            Response Type 2
          </button>
          <button onMouseDown={(e) => addResponseType(e, "type3")}>
            Response Type 3
          </button>
        </div>
      </ResponseTypeContainerTool>
      <ResponseTypeContainer className="fill-the-gap">
        <ContainerEditor getPos={getPos} node={node} view={view} />
      </ResponseTypeContainer>
    </ResponseTypeWrapper>
  );
};

export default ResponseTypeContainerComponent;

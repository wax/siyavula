const fillTheGapContainerNode = {
  attrs: {
    id: { default: "" },
    class: { default: "fill-the-gap" },
  },
  group: "block questions",
  atom: true,
  draggable: false,
  selectable: true,
  defining: true,
  content: "block*",
  parseDOM: [
    {
      tag: "div.fill-the-gap",
      getAttrs(dom) {
        return {
          id: dom.getAttribute("id"),
          class: dom.getAttribute("class"),
        };
      },
    },
  ],
  toDOM(node) {
    return ["div", node.attrs, 0];
  },
};

export default fillTheGapContainerNode;

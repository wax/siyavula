const fillTheGapNode = {
  attrs: {
    id: { default: "" },
    class: { default: "fill-the-gap" },
    value: { default: "" },
    type: { defalut: "" },
  },
  group: "inline",
  inline: true,
  excludes: "fill_the_gap",
  parseDOM: [
    {
      tag: "span.fill-the-gap",
      getAttrs(dom) {
        return {
          id: dom.getAttribute("id"),
          class: dom.getAttribute("class"),
          value: dom.getAttribute("value"),
          type: dom.getAttribute("type"),
        };
      },
    },
  ],
  toDOM: (node) => {
    return ["span", node.attrs];
  },
};

export default fillTheGapNode;

import React from "react";
import { Wax } from "wax-prosemirror-core";
import config from "./config";
import Layout from "./layout/layout";

const Siyavula = () => {
  return (
    <Wax
      autoFocus
      config={config}
      layout={Layout}
      targetFormat="JSON"
      onChange={(source) => console.log(source)}
    />
  );
};

export default Siyavula;

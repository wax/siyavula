import {
  InlineAnnotationsService,
  AnnotationToolGroupService,
  ImageService,
  ImageToolGroupService,
  LinkService,
  ListsService,
  ListToolGroupService,
  TablesService,
  TableToolGroupService,
  BaseService,
  BaseToolGroupService,
  MathService,
  FullScreenService,
  FullScreenToolGroupService,
} from "wax-prosemirror-services";

import { DefaultSchema } from "wax-prosemirror-core";

import ResponseTypeService from "./ResponseTypeService/ResponseTypeService";

const config = {
  SchemaService: DefaultSchema,

  services: [
    new ListsService(),
    new LinkService(),
    new InlineAnnotationsService(),
    new ImageService(),
    new TablesService(),
    new BaseService(),
    new BaseToolGroupService(),
    new TableToolGroupService(),
    new ImageToolGroupService(),
    new AnnotationToolGroupService(),
    new ListToolGroupService(),
    new MathService(),
    new FullScreenService(),
    new FullScreenToolGroupService(),
    new ResponseTypeService(),
  ],
};
export default config;
